import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Equipos} from '../interfaces/interfaces';
import { Partidos } from '../interfaces/interfaces';
import { Goleadores } from '../interfaces/interfaces';


@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }

  getEquipos(){
    return this.http.get<Equipos>('https://app.sportdataapi.com/api/v1/soccer/teams?apikey=bd6c55e0-5dd4-11ec-891b-cddaabd5c8fe&country_id=113')
  }

  getPartidos(){
    return this.http.get<Partidos>('https://app.sportdataapi.com/api/v1/soccer/matches?apikey=bd6c55e0-5dd4-11ec-891b-cddaabd5c8fe&season_id=2029&date_from=2022-01-16')
  }

  getGoleadores(){
    return this.http.get<Goleadores>('https://app.sportdataapi.com/api/v1/soccer/topscorers?apikey=bd6c55e0-5dd4-11ec-891b-cddaabd5c8fe&season_id=2029')
  }

  getEquipoDetalle(id){
    return this.http.get<Equipos>(`https://app.sportdataapi.com/api/v1/soccer/teams/${id}?apikey=bd6c55e0-5dd4-11ec-891b-cddaabd5c8fe`)
  }
  
}
