import { Injectable } from '@angular/core';
import { Datum } from '../interfaces/interfaces';
import { Storage } from '@ionic/storage-angular';

@Injectable({
  providedIn: 'root'
})
export class DataLocalService {
  equipos: Datum[]=[];
  private _storage: Storage | null = null;
  constructor(private storage: Storage) {
   this.init()
  }
  async init() {
   const storage = await this.storage.create();
   this._storage = storage;
  }

 async getData(){
  const data = await this.storage.get('equipos')
  return data
 }

async addEquipo(equipo){
  this.equipos.push(equipo)
  this.storage.set('equipos', this.equipos)
  console.log(this.equipos)
}

async delEquipo(equipo){
  this.equipos = this.equipos.filter(item=>item.team_id!=equipo)
  this.storage.set('equipos', this.equipos)

}

 
}

