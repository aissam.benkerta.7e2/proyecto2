import { Component, Input } from '@angular/core';
import { DataService } from '../services/data.service';
import { Datum } from '../interfaces/interfaces';
import { ModalController } from '@ionic/angular';
import { DetalleComponent } from '../components/detalle/detalle.component';


@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  @Input() id
  equipos : Datum[]=[];
  filterItems:any;

  constructor(private data: DataService, private modalCtrl: ModalController) {}

  ngOnInit(): void {
    this.loadEquipos();
    
  }
  loadEquipos(event?){
    this.data.getEquipos().subscribe(
      resp => {
        this.equipos = resp.data;
        this.filterItems = this.equipos
        if(event){
          event.target.complete();
        }
      }
    )
  }

  filtrarItems(name){
    if( name !=''){
      this.filterItems = this.equipos.filter(item =>  item.name.toLowerCase().indexOf(name.toLowerCase()) > -1);
    }
    else{
      this.filterItems = this.equipos;
    }

    }

    async verDetalle(id: string){
      const modal = await this.modalCtrl.create({
        component: DetalleComponent,
        componentProps: {id}
      })
      modal.present()
    }



}
