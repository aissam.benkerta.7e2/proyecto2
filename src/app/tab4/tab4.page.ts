import { Component, OnInit } from '@angular/core';
import { Datum } from '../interfaces/interfaces';
import { DataLocalService } from '../services/data-local.service';

@Component({
  selector: 'app-tab4',
  templateUrl: './tab4.page.html',
  styleUrls: ['./tab4.page.scss'],
})
export class Tab4Page implements OnInit {
  equipos : Datum[]=[];
  filterItems:any;
  constructor(private data: DataLocalService,private dataLocal: DataLocalService) { }

  async ngOnInit() {
    
    this.equipos = await this.data.getData();
    this.filterItems = this.equipos
    
  } 
  async ionViewWillEnter(){
    this.equipos = await this.data.getData();
    this.filterItems = this.equipos
  }

  filtrarItems(name){
    if( name !=''){
      this.filterItems = this.equipos.filter(item =>  item.name.toLowerCase().indexOf(name.toLowerCase()) > -1);
    }
    else{
      this.filterItems = this.equipos;
    }

    }

  delFav(equipo){
    this.dataLocal.delEquipo(equipo)
    this.ngOnInit()
  }

}
