

export interface Equipos {
  query: Query;
  data: Datum[];
}

export interface Datum {
  team_id: number;
  name: string;
  short_code: string;
  common_name?: string;
  logo: string;
  country: Country;
}

export interface Query {
  apikey: string;
  country_id: string;
}

export interface Partidos {
  query: Query2;
  data: Datum2[];
}

export interface Datum2 {
  match_id: number;
  status_code: number;
  status: string;
  match_start: string;
  match_start_iso: string;
  minute?: number;
  league_id: number;
  season_id: number;
  stage: Stage;
  group: Group;
  round: Round;
  referee_id?: number;
  home_team: Hometeam;
  away_team: Hometeam;
  stats: Stats;
  venue: Venue;
}

export interface Venue {
  venue_id: number;
  name: string;
  capacity: number;
  city: string;
  country_id: number;
}

export interface Stats {
  home_score: number;
  away_score: number;
  ht_score?: string;
  ft_score?: string;
  et_score?: any;
  ps_score?: any;
}

export interface Hometeam {
  team_id: number;
  name: string;
  short_code: string;
  common_name: string;
  logo: string;
  country: Country;
}

export interface Country {
  country_id: number;
  name: string;
  country_code: string;
  continent: string;
}

export interface Round {
  round_id: number;
  name: string;
  is_current?: number;
}

export interface Group {
  group_id: number;
  group_name: string;
}

export interface Stage {
  stage_id: number;
  name: string;
}

export interface Query2 {
  apikey: string;
  season_id: string;
  date_from: string;
}

export interface Goleadores {
  query: Query3;
  data: Datum3[];
}

export interface Datum3 {
  pos: number;
  player: Player;
  team: Team;
  league_id: number;
  season_id: number;
  matches_played: number;
  minutes_played: number;
  substituted_in?: number;
  goals: Goals;
  penalties?: number;
}

export interface Goals {
  overall: number;
  home: number;
  away: number;
}

export interface Team {
  team_id: number;
  team_name: string;
}

export interface Player {
  player_id?: number;
  player_name?: string;
}

export interface Query3 {
  apikey: string;
  season_id: string;
}