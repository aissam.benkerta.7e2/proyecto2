import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DetalleComponent } from 'src/app/components/detalle/detalle.component';



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  exports:[
    DetalleComponent
  ]
})
export class ComponentsModule { }
