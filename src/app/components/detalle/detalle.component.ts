import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { DataLocalService } from 'src/app/services/data-local.service';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.scss'],
})
export class DetalleComponent implements OnInit {
  @Input() id
  equipo: any;

  constructor(private modalCtrl: ModalController, private data: DataService,private dataLocal: DataLocalService) { }

  ngOnInit() {
    this.data.getEquipoDetalle(this.id).subscribe(
      resp =>{
        this.equipo = resp.data;
      }
    )
  }
  regresar(){
    this.modalCtrl.dismiss()
  }

  addFav(equipo){
    this.dataLocal.addEquipo(equipo)
  }
}
