import { Component } from '@angular/core';
import { DataService } from '../services/data.service';
import { Datum3 } from '../interfaces/interfaces';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  goleadores : Datum3[]=[];
  filterItems:any;

  constructor(private data: DataService) {}

  ngOnInit(): void {
    this.loadGoleadores();
    
  }
  loadGoleadores(event?){
    this.data.getGoleadores().subscribe(
      resp => {
        this.goleadores = resp.data;
        this.filterItems = this.goleadores;
        if(event){
          event.target.complete();
        }
        console.log(this.goleadores)
      }
    )
  }
  loadData(event){
    this.loadGoleadores(event)
  }

  filtrarItems(name){
    if( name !=''){
      this.filterItems = this.goleadores.filter(item =>  item.player.player_name.toLowerCase().indexOf(name.toLowerCase()) > -1);
    }
    else{
      this.filterItems = this.goleadores;
    }

    }

}
