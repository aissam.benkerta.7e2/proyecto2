import { Component } from '@angular/core';
import { DataService } from '../services/data.service';
import { Datum2 } from '../interfaces/interfaces';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  partidos : Datum2[]=[];
  searchTerm: string ;
  filterItems:any;
  
  constructor(private data: DataService) {}

  ngOnInit(): void {
    this.loadPartidos();
    
  }
  loadPartidos(event?){
    this.data.getPartidos().subscribe(
      resp => {
        this.partidos = resp.data;
        this.filterItems = this.partidos
        if(event){
          event.target.complete();
        }
      }
    )
  }
  loadData(event){
    this.loadPartidos(event)
  }

  filtrarItems(name){
    if( name !=''){
      this.filterItems = this.partidos.filter(item =>  item.home_team.name.toLowerCase().indexOf(name.toLowerCase()) > -1);
    }
    else{
      this.filterItems = this.partidos;
    }

    }

}
